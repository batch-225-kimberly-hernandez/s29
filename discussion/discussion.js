// Inserting data through our database
db.users.insertMany([
    {
        firstName: "Jane",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "87654321",
            email: "janedoe@gmail.com"
        },
        courses: ["CSS", "Javascript", "Python"],
        department: "none"
    },
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 50,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: ["React", "Laravel", "Sass"],
        department: "none"
    }
]);

// [SECTION] Comparison Query Operators

// $gt/$gte operator
// - Allows us to find documents that have field number value greatar than or equal to a specified value

db.users.find(
    {
        age: {
            $gt: 50
        }

    }
)
db.users.find(
    {
        age: {
            $gte: 50
        }

    }
)

// $lt/$lte operator
// - Allows us to find documents that have field number value lesser than or equal to a specified value

db.users.find(
    {
        age: {
            $lt: 50
        }

    }
)
db.users.find(
    {
        age: {
            $lte: 50
        }

    }
)
// $ne operator
// - Allows us to find documents that have field number values not equal to a specified value
db.users.find(
    {
        age: {
            $ne: 82
        }

    }
)

// $in operator


/*
    - Allows us to find documents with specific match criteria one field using different values.
    - Syntax:
        db.collectioName.find({field:{$in:value}})

*/

db.users.find({ lastnName: { $in: ["Hawking", "Doe"] } });
db.users.find({ courses: { $in: ["HTML", "React"] } })


// [SECTION] Logical Query Operators
// $or operator
/*
- Allows us to find documents that match a single criteria from multiple provided search criteria.
    - Syntax
        db.collectionName.find({ $or: [ { fieldA: valueB }, { fieldB: valueB } ] });
*/
db.users.find({ $or: [{ firstName: "Neil" }, { age: 50 }] });
db.users.find({ $or: [{ firstName: "Neil" }, { age: { $gt: 50 } }] });

// $and operator
/*
    - Allows us to find documents matching multiple criteria in a single field.

*/

db.users.find({ $and: [{ age: { $ne: 82 } }, { age: { $ne: 50 } }] })

// [SECTION] Field Projection

/*
    - Retrieving documents are common operators that we do and by default MongoDB queries return the whole document as a response.
    - When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.
    - To help with readability of the values returned, we can include/exclude fields from the response.
*/

// Inclusion

/*
    - Allows us to include/add specific fields only when retrieving documents.
    - The value provided is 1 or "true" to denote that field is being included.
    Syntax:

        db.users.find({criteria}, {field:1})

*/

db.users.find(
    {
        firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 1,
        contact: 1
    }

)

// Inclusion

/*
    - Allows us to include/add specific fields only when retrieving documents.
    - The value provided is 0 or "false" to denote that field is being included.
    Syntax:

        db.users.find({criteria}, {field:1})

*/

db.users.find(
    {
        firstName: "Stephen"
    },
    {
        contact: 0,
        department: 0,
        courses: 0
    }

)

// [SECTION] Evaluation Query Operators

// $regex operator
/*
    - Allows us to find documents that match a specific string pattern using regular expressions.
    - $options: '$i' - is for case insensitivity 

    - $options: '$x' - for "extended" capability to ignore all white spaces characters in the $regex pattern unless escaped or included in a character class.
*/

// Case senstivy query

db.users.find({ firstName: { $regex: 'N' } })

// Case insensitivity query
db.users.find({ firstName: { $regex: 'j', $options: '$i' } })