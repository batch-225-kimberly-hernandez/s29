db.users.insertMany([
    {
        firstName: "Jane",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "87654321",
            email: "janedoe@gmail.com"
        },
        courses: ["CSS", "Javascript", "Python"],
        department: "none"
    },
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 50,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: ["React", "Laravel", "Sass"],
        department: "none"
    }
]);


// Step 2, find firstname with s and last name with d

db.users.find(
    {

        $or: [{ firstName: { $regex: 's', $options: '$i' } },
        { lastName: { $regex: 'd', $options: '$i' } }]

    },
    {
        firstName: 1,
        lastName: 1,
        _id: 0
    }
)
// Step 3, Find users who are from the HR department and their age is greater than or equal to 70.
db.users.find({
    $and: [{ department: 'HR' }, { age: { $gte: 70 } }]
})




// Step 4, Find users with the letter e in their first name and has an age of less than or equal to 30.
db.users.find({
    $and: [{ firstName: { $regex: 'e', $options: '$i' } }, { age: { $lte: 30 } }]
})